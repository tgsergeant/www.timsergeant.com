# www.timsergeant.com

This is my personal site. I don't update it much, but here we are.

## Getting Started

1. [Install Rust](https://www.rust-lang.org/tools/install): `curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh`

2. Install Cobalt: `cargo install cobalt-bin`

3. Build: `cobalt build`

4. Get Firebase:

   a. Install node: [See Nodesource](https://github.com/nodesource/distributions/blob/master/README.md)

   b. Install yarn: https://classic.yarnpkg.com/en/docs/install#debian-stable

   c. `yarn global add firebase-tools` (and make sure `yarn bin` is on your `$PATH`)

   d. `firebase login`

5. `firebase deploy`
