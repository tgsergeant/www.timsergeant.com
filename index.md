---
layout: homepage.liquid
permalink: /
title: the Tim Sergeant zone
---

<h1 id="nameheader">Tim Sergeant</h1>

[tim@sergeant.zone](mailto:tim@sergeant.zone)

I'm a software engineer based in Sydney. I work at [Google](https://www.google.com/) on [ChromeOS](https://www.google.com/chromebook/chrome-os/), building better integrations between different types of apps. Most of my ChromeOS work is [open source](https://chromium-review.googlesource.com/q/(author:tsergeant%2540chromium.org+OR+reviewer:tsergeant%2540chromium.org)+is:merged). I've also worked on [Google Go](https://play.google.com/store/apps/details?id=com.google.android.apps.searchlite), the [Android Google App](https://play.google.com/store/apps/details?id=com.google.android.googlequicksearchbox), and the [Chrome browser](https://www.chromium.org/).

Before Google, I interned at [Bigcommerce](https://www.bigcommerce.com/), and I studied Computer Science at [ANU](https://www.anu.edu.au/). I spent my [honours year](/pages/hhvm/) studying memory management in [HHVM](https://www.hhvm.com).
