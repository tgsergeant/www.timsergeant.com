---
title: PLTL-MUP
---

Minimal Unsatisfiability Prover for Propositional Linear Temporal Logic.

By Rajeev Goré, Jinbo Huang, Timothy Sergeant and Jimmy Thomson.

## Downloads

* Paper: [Finding Minimal Unsatisfiable Subsets in Linear Temporal Logic using BDDs](/files/pltlmup/gore_huang_sergeant_thomson_mus_pltl.pdf)
* Source Code: [pltl-mup.tar.gz](/files/pltlmup/pltl-mup.tar.gz)
* Test data and logs: [pltl-mup-tests.tar.gz](/files/pltlmup/pltl-mup-tests.tar.gz)

## Abstract

When creating a knowledge base of linear temporal logic formulae as
a specification, it is easy to introduce inconsistency, particularly
if the knowledge base is created by multiple parties. Identifying
the exact formulae which lead to inconsistency is a difficult but
important problem which allows us to diagnose the source of the
error more easily. We describe a method which uses binary decision
diagrams to find a minimal unsatisfiable subset of a given
inconsistent knowledge base, thereby removing any formulae which do
not directly contribute to the inconsistency. Our method extends an
existing BDD-based method for finding minimal unsatisfiable subsets
in classical propositional logic and requires only a single call to
a BDD-based theorem prover to operate. We also present a more
efficient method which uses an existing resolution-based method to
first find a, not necessarily minimal, unsatisfiable subset and then
uses our BDD-based method to refine this set to a minimal
unsatisfiable subset of the knowledge base.
